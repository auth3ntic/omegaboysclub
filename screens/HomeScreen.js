import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, Button } from 'react-native';
import { WebBrowser } from 'expo';

class StaffScreen extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    result: null,
  };
    render() {
        return (
            <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.contentContainer}>
            <Text style={styles.titleText}>The Prescription to End Violence and Change Lives</Text>
              <View style={styles.bodyContainer}>
              <Image
                source={require('../assets/images/staff/club.jpg')}
                style={{ width: 300, height: 200, borderWidth: 1, borderRadius: 25 }}
                />
                <Button 
                  style={styles.button} 
                  title="Generations of Alive & Free Omega" 
                  onPress={this._infoPressButtonAsync} />
              </View>
              <View style={styles.bodyContainer}>
                  <Image
                    source={require('../assets/images/staff/cofounders.jpg')}
                    style={{ width: 300, height: 200, borderWidth: 1, borderRadius: 25 }}
                  />
                  <Text style={styles.main}>Jack Jacqua & Dr. Marshall</Text>
                </View>

                <View style={styles.bodyContainer}>
                  <Image
                    source={require('../assets/images/staff/andre.jpg')}
                    style={{ width: 300, height: 300, borderWidth: 1, borderRadius: 25 }}
                  />
                  <Text style={styles.main}>Mr. Andre Aikins</Text>
                </View>

                <View style={styles.bodyContainer}>
                <Image
                  source={require('../assets/images/staff/anf.jpg')}
                  style={{ width: 300, height: 200, borderWidth: 1, borderRadius: 25 }}
                  />
                  <Text style={styles.main}>Ms. Estell & Mrs. Patterson</Text>
                </View>
                <View style={styles.bodyContainer}>
                <Image
                  source={require('../assets/images/staff/demetra.jpg')}
                  style={{ width: 300, height: 200, borderWidth: 1, borderRadius: 25 }}
                />
                  <Text style={styles.main}>Ms. Demetra Jones</Text>
                </View>
                <View style={styles.bodyContainer}>
                  <Image
                  source={require('../assets/images/staff/ann.jpg')}
                  style={{ width: 300, height: 300, borderWidth: 1, borderRadius: 25 }}
                  />
                  <Text style={styles.main}>Ms. Ann Bassette</Text>
                </View>
                  
            </ScrollView>
            </View>
        );
    }
    _infoPressButtonAsync = async () => {
      let result = await WebBrowser.openBrowserAsync('https://stayaliveandfree.org/');
      this.setState({ result });
    };
}


export default StaffScreen;

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      paddingHorizontal: 2,
    },
    introContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    bodyContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
        padding: 10,
        marginTop: 20,
    },
    button: {
      backgroundColor: '#fff',
      color: 'blue',
      padding: 5,
    },
    contentContainer: {
        paddingHorizontal: 2,
        margin: 2,
    },
    infoText: {
      fontSize: 16,
      color: 'rgba(0,0,255, 1)',
      lineHeight: 24,
      textAlign: 'center',
      textDecorationLine: 'underline'
    },
    main: {
        fontSize: 16,
        color: 'rgba(190,37,32, 1)',
        lineHeight: 24,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    titleText: {
        fontSize: 24,
        color: 'rgba(190,37, 22, 1)',
        lineHeight: 24,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});